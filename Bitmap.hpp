#ifndef __BITMAP_HPP__
#define __BITMAP_HPP__

#include <vector>
#include <fstream>

class Bitmap {
private:
    typedef struct __attribute__((packed)) {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    } pixel_t;

    uint32_t height;
    
    uint32_t width;
    
    uint32_t padding;

    std::vector<std::vector<uint8_t>> grayscale;

public:
    Bitmap(uint32_t, uint32_t);

    void extract_bitmap(std::ifstream&);

    void write(std::string& filename, std::string& fmt);
    
};

#endif /* __BITMAP_HPP__ */