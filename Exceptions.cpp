#include "Exceptions.hpp"

Exception::Exception(const char* message) 
{
    msg = message;
}

const char* Exception::what() const noexcept 
{
   return msg.c_str();
}
