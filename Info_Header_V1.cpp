#include "Info_Header_V1.hpp"
#include <iostream>

Info_Header_V1::Info_Header_V1(std::ifstream& s) 
{
    underlying_data = new char [sizeof(infoheaderv1_t)];
    s.read(underlying_data, sizeof(infoheaderv1_t));
    header = reinterpret_cast<infoheaderv1_t *>(underlying_data);
}

Info_Header_V1::~Info_Header_V1() 
{
    delete [] underlying_data;
}

void Info_Header_V1::print(void)
{
    std::cout << name << std::endl;
    std::cout << "Image width: " << header->width << " px" << std::endl;
    std::cout << "Image heigh: " << header->height << " px" << std::endl;
    std::cout << "Bit count: " << header->bit_count << " bits/px" << std::endl;
    std::cout << "Compression: " << compression_val2name(header->compression) << std::endl;
    std::cout << "Image size: " << header->image_size << " bytes" << std::endl;
    std::cout << "Resolution horizontal: " << header->x_px_per_m << " px/m" << std::endl;
    std::cout << "Resolution vertical: " << header->y_px_per_m << " px/m" << std::endl;
    std::cout << "Colors used: " << header->colors_used << std::endl;
    std::cout << "Colors used: " << header->colors_important << std::endl;
}

bool Info_Header_V1::validate(void) 
{
    if (header->compression != 0) {
        std::cout << "Following compression type does not supports: " << compression_val2name(header->compression) << std::endl;
        return false;
    }
    if (header->bit_count != 24) {
        std::cout << "Following bit count does not supports: " << header->bit_count << std::endl;
        return false;
    }
    return true;
}

void Info_Header_V1::get_dimensions(uint32_t& width, uint32_t& height)
{
    width = header->width;
    height = header->height;
}
