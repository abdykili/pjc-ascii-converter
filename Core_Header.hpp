#ifndef __CORE_HEADER_HPP__
#define __CORE_HEADER_HPP__

#include <fstream>
#include <iostream>

class Core_Header {
private:
    char *underlying_data;

    typedef struct __attribute__((packed)){
        uint8_t signature[2];
        uint32_t file_size;
        uint32_t __unused;
        uint32_t bitmap_offset;
    } core_header_str;
    
    core_header_str *header;

public:
    Core_Header(std::ifstream&);
    
    ~Core_Header();
    
    bool validate(void);

    void print(void);

    int get_bitmap_offset(void);
};

#endif /* __CORE_HEAER_HPP__ */
