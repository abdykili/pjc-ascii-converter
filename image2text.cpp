#include <iostream>
#include <string>
#include <BMP_File.hpp>

void display_help(const char *argv)
{
    std::cout << "Usage: " << argv << " file_in "
    << "[file_out] [fmt]" << std::endl;
    std::cout << "\tfile_in   - input .bmp file" << std::endl;
    std::cout << "\tfile_out  - output .txt file" << std::endl;
    std::cout << "\tfmt       - formatter for text image" << std::endl;
}

int main(int argc, char *argv[]) 
{
    std::string in, out, fmt;
    if (argc < 2) {
        display_help(argv[0]);
        return 1;    
    } else if (std::string(argv[1]) == "--help"){
        display_help(argv[0]); 
        return 0;
    } else {
        argv++;
        argc--;
        in = std::string(argv[0]);
        out = std::string((argc > 1) ? argv[1] : "out.txt");
        fmt = std::string((argc > 2) ? argv[2] : " .:io+#@");
    }
    int fmtlen = fmt.length();
    if ((fmtlen == 0) || (fmtlen & (fmtlen - 1))) {
        std::cout << "Formatter length must be power of 2" << std::endl;
        return 1; 
    }
    BMP_File bmpfile(in);
    if (bmpfile.extract()) {
        bmpfile.create_file(out, fmt);
    } else {
        return 1;
    }
    return 0;
}