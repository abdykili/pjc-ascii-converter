#ifndef __INFO_HEADER_V1_HPP__
#define __INFO_HEADER_V1_HPP__

#include <Info_Header_Base.hpp>

class Info_Header_V1 : public Info_Header_Base{
    private:

        const char *name = "BITMAPINFOHEADER";

        char *underlying_data;

        typedef struct __attribute__((packed)){
            int32_t width;
            int32_t height;
            uint16_t plane_count;
            uint16_t bit_count;
            uint32_t compression;
            uint32_t image_size;
            uint32_t x_px_per_m;
            uint32_t y_px_per_m;
            uint32_t colors_used;
            uint32_t colors_important;
        } infoheaderv1_t;

        infoheaderv1_t *header;

    public:
        Info_Header_V1(std::ifstream&);

        ~Info_Header_V1(void);

        void print(void);

        bool validate(void);

        void get_dimensions(uint32_t&, uint32_t&);
};

#endif /* __INFO_HEADER_V1_HPP__ */
