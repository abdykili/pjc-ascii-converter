#ifndef __INFO_HEADER_BASE_HPP__
#define __INFO_HEADER_BASE_HPP__

#include <fstream>

class Info_Header_Base {
    private:

        char *underlying_data = nullptr;

        typedef enum : uint32_t {
            BI_RGB,
            BI_RLE8,
            BI_RLE4,
            BI_BITFIELDS,
            BI_JPEG,
            BI_PNG,
            BI_ALPHABITGIELDS,
            BI_CMYK = 11,
            BI_CMYKRLE8,
            BI_CMYKRLE4
        } compression_t;

    public:
        Info_Header_Base() {}

        ~Info_Header_Base() {}

        virtual void print(void) = 0;

        virtual bool validate(void) = 0;

        const char *compression_val2name(uint32_t);

        virtual void get_dimensions(uint32_t&, uint32_t&) = 0;
};

#endif /* __INFO_HEADER_BASE_HPP__ */
