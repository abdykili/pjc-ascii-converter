#ifndef __EXCEPTION_HPP__
#define __EXCEPTION_HPP__

#include <exception>
#include <string>

class Exception: public std::exception {
public:
    explicit Exception(const char* message) : msg(message) {}

    virtual ~Exception() noexcept {}

    virtual const char* what() const noexcept {
        return msg.c_str();
    }

protected:
    std::string msg;
};

#endif /* __EXCEPTION_HPP__ */