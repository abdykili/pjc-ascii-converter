#include "Info_Header_Base.hpp"
#include <iostream>

#define COMPRESSION_COUNT (10)

static const char *compression_name[COMPRESSION_COUNT] = {
    [0] = "BI_RGB",
    [1] = "BI_RLE8",
    [2] = "BI_RLE4",
    [3] = "BI_BITFIELDS",
    [4] = "BI_JPEG",
    [5] = "BI_PNG",
    [6] = "BI_ALPHABITGIELDS",
    [7] = "BI_CMYK = 11",
    [8] = "BI_CMYKRLE8",
    [9] = "BI_CMYKRLE4"
};

const char *Info_Header_Base::compression_val2name(uint32_t compression)
{
    switch (static_cast<compression_t>(compression)) {
        case BI_RGB:
            return compression_name[0];
        case BI_RLE8:
            return compression_name[1];
        case BI_RLE4:
            return compression_name[2];
        case BI_BITFIELDS:
            return compression_name[3];
        case BI_JPEG:
            return compression_name[4];
        case BI_PNG:
            return compression_name[5];
        case BI_ALPHABITGIELDS:
            return compression_name[6];
        case BI_CMYK:
            return compression_name[7];
        case BI_CMYKRLE8:
            return compression_name[8];
        case BI_CMYKRLE4:
            return compression_name[9];
        default:
            return NULL;
    }
}
