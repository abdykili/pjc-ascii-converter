#include "Core_Header.hpp"
#include <iostream>

Core_Header::Core_Header(std::ifstream& s) 
{
    this->underlying_data = new char [sizeof(core_header_str)];
    s.read(underlying_data, sizeof(core_header_str));
    header = reinterpret_cast<core_header_str *>(underlying_data);
};

Core_Header::~Core_Header() 
{
    delete [] this->underlying_data;
};

bool Core_Header::validate(void)
{
    return ((header->signature[0] == 'B') && (header->signature[1] == 'M') && !(header->__unused));
}

void Core_Header::print(void)
{
    std::cout << "BITMAPCOREHEADER:" << std::endl;
    std::cout << "Signature: " << header->signature[0] << header->signature[1] << std::endl;
    std::cout << "File size: " << header->file_size << " bytes" << std::endl;
    std::cout << "Bitmap offset: " << header->bitmap_offset << " bytes" << std::endl;
}

int Core_Header::get_bitmap_offset(void) 
{
    return header->bitmap_offset;
}
