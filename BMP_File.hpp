#ifndef __BMP_FILE_HPP__
#define __BMP_FILE_HPP__

#include <memory>
#include <vector>
#include "Core_Header.hpp"
#include "Info_Header_V1.hpp"
#include "Info_Header_V4.hpp"
#include "Info_Header_V5.hpp"
#include "Exceptions.hpp"
#include "Bitmap.hpp"


class BMP_File {
private:
    std::ifstream fstream;
    
    std::shared_ptr<Core_Header> core_header = nullptr;
    
    std::shared_ptr<Info_Header_Base> info_header = nullptr;

    void extract_core_header(void);

    void extract_info_header(void);

    void extract_bitmap(void);

public:
    std::shared_ptr<Bitmap> bitmap = nullptr;

    BMP_File(std::string&);
    
    ~BMP_File() {}
    
    bool extract(void);

    void create_file(std::string&, std::string&);

};


#endif /* __BMP_FILE_HPP__ */