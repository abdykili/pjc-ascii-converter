#ifndef __INFO_HEADER_V5_HPP__
#define __INFO_HEADER_V5_HPP__

#include <Info_Header_Base.hpp>

class Info_Header_V5 : public Info_Header_Base{
    private:

        const char *name = "BITMAPV5HEADER";

        char *underlying_data;

        typedef struct __attribute__((packed)){
            uint32_t x;
            uint32_t y;
            uint32_t z;
        } colorxyz_t;

        typedef struct __attribute__((packed)){
            colorxyz_t red;
            colorxyz_t green;
            colorxyz_t blue;
        } endpoints_t;

        typedef struct __attribute__((packed)){
            int32_t width;
            int32_t height;
            uint16_t plane_count;
            uint16_t bit_count;
            uint32_t compression;
            uint32_t image_size;
            uint32_t x_px_per_m;
            uint32_t y_px_per_m;
            uint32_t colors_used;
            uint32_t colors_important;
            uint32_t red_mask;
            uint32_t green_mask;
            uint32_t blue_mask;
            uint32_t alpha_mask;
            uint32_t color_space;
            endpoints_t endpoints;
            uint32_t gamma_red;
            uint32_t gamma_green;
            uint32_t gamma_blue;
            uint32_t intent;
            uint32_t profile_offset;
            uint32_t profile_size;
            uint32_t __unused;
        } infoheaderv5_t;

        infoheaderv5_t *header;

    public:
        Info_Header_V5(std::ifstream&);

        ~Info_Header_V5(void);

        void print(void);

        bool validate(void);

        void get_dimensions(uint32_t&, uint32_t&);
};

#endif /* __INFO_HEADER_V5_HPP__ */
