#include "BMP_File.hpp"

#define INFOHEADER_COUNT (6)

typedef enum : uint32_t {
    OS22XBITMAPHEADER = 64,
    BITMAPINFOHEADER = 40,
    BITMAPV2INFOHEADER = 52,
    BITMAPV3INFOHEADER = 56,
    BITMAPV4HEADER = 108,
    BITMAPV5HEADER = 124
} infoheader_size_t;

static const char *bitmapinfoheader_name[INFOHEADER_COUNT] = {
    [0] = "OS22XBITMAPHEADER",
    [1] = "BITMAPINFOHEADER",
    [2] = "BITMAPV2INFOHEADER",
    [3] = "BITMAPV3INFOHEADER",
    [4] = "BITMAPV4HEADER",
    [5] = "BITMAPV5HEADER"
};

static const char *infoheader_size2name(infoheader_size_t size) {
    switch (size) {
        case OS22XBITMAPHEADER:
            return bitmapinfoheader_name[0]; 
        case BITMAPINFOHEADER:
            return bitmapinfoheader_name[1];
        case BITMAPV2INFOHEADER:
            return bitmapinfoheader_name[2];
        case BITMAPV3INFOHEADER:
            return bitmapinfoheader_name[3];
        case BITMAPV4HEADER:
            return bitmapinfoheader_name[4];
        case BITMAPV5HEADER:
            return bitmapinfoheader_name[5];
        default:
            return NULL;
    }
}

BMP_File::BMP_File(std::string& filename)
{
    fstream = std::ifstream(filename, std::ifstream::binary);
}

void BMP_File::extract_core_header(void) 
{
    if (fstream.is_open()) {
        core_header = std::shared_ptr<Core_Header>(new Core_Header(fstream));
        core_header->print();
        if (!core_header->validate()) {
            throw Exception("An error occured during parsing BITMAPCOREHEADER");
        }
    } else {
        throw Exception("File stream has been unexpectacly closed");
    }
}

void BMP_File::extract_info_header(void)
{
    if (fstream.is_open()) {
        char buf[sizeof(uint32_t)];
        fstream.read(buf, sizeof(uint32_t));
        infoheader_size_t *info_header_size = reinterpret_cast<infoheader_size_t *>(buf);
        switch (*info_header_size) {
        case BITMAPINFOHEADER:
            info_header = std::shared_ptr<Info_Header_V1>(new Info_Header_V1(fstream));
            break;
        case BITMAPV4HEADER:
            info_header = std::shared_ptr<Info_Header_V4>(new Info_Header_V4(fstream));
            break;
        case BITMAPV5HEADER:
            info_header = std::shared_ptr<Info_Header_V5>(new Info_Header_V5(fstream));
            break;
        default:
            std::cout << "Following BMP header does not supports: " << infoheader_size2name(*info_header_size) << std::endl;
            break;
        }
        if (info_header != nullptr) {
            info_header->print();
            if (!info_header->validate()) {
                throw Exception("An error occured during parsing BITMAPINFOHEADER");
            }
        } else {
            throw Exception("An error occured during determing BITMAPCOREHEADER type");
        }
    } else {
        throw Exception("File stream has been unexpectacly closed");
    }
}

void BMP_File::extract_bitmap(void)
{
    uint32_t width, height;
    info_header->get_dimensions(width, height);
    bitmap = std::shared_ptr<Bitmap>(new Bitmap(width, height));
    uint32_t offset = core_header->get_bitmap_offset();
    fstream.seekg(offset, std::ios::beg);
    bitmap->extract_bitmap(fstream);
}

bool BMP_File::extract(void) 
{
    try {
        extract_core_header();
        extract_info_header();
        extract_bitmap();
        if (fstream.is_open()) {
            fstream.close();
        }
    } catch(Exception& e) {
        std::cout << e.what() << "\nExiting..." << std::endl;
        return false;
    }
    return true;
}

void BMP_File::create_file(std::string& filename, std::string& fmt) {
    bitmap->write(filename, fmt);
}