#include "Info_Header_V4.hpp"
#include <iostream>

Info_Header_V4::Info_Header_V4(std::ifstream& s) 
{
    underlying_data = new char [sizeof(infoheaderv4_t)];
    s.read(underlying_data, sizeof(infoheaderv4_t));
    header = reinterpret_cast<infoheaderv4_t *>(underlying_data);
}

Info_Header_V4::~Info_Header_V4() 
{
    delete [] underlying_data;
}

void Info_Header_V4::print(void)
{
    std::cout << name << std::endl;
    std::cout << "Image width: " << header->width << " px" << std::endl;
    std::cout << "Image heigh: " << header->height << " px" << std::endl;
    std::cout << "Bit count: " << header->bit_count << " bits/px" << std::endl;
    std::cout << "Compression: " << compression_val2name(header->compression) << std::endl;
    std::cout << "Image size: " << header->image_size << " bytes" << std::endl;
    std::cout << "Resolution horizontal: " << header->x_px_per_m << " px/m" << std::endl;
    std::cout << "Resolution vertical: " << header->y_px_per_m << " px/m" << std::endl;
    std::cout << "Colors used: " << header->colors_used << std::endl;
    std::cout << "Colors used: " << header->colors_important << std::endl;
    std::cout << "Red mask: 0x" << std::hex <<  header->red_mask << std::endl;
    std::cout << "Green mask: 0x" << std::hex << header->red_mask << std::endl;
    std::cout << "Blue mask: 0x" << std::hex << header->red_mask << std::endl;
    std::cout << "Alpha mask: 0x" << std::hex << header->red_mask << std::endl;
    std::cout << "Color space: " << header->color_space << std::endl;
    std::cout << "Endpoint red: (" << header->endpoints.red.x << ", " << header->endpoints.red.y << ", " << header->endpoints.red.z <<  ")" << std::endl;
    std::cout << "Endpoint green: (" << header->endpoints.green.x << ", " << header->endpoints.green.y << ", " << header->endpoints.green.z <<  ")" << std::endl;
    std::cout << "Endpoint blue: (" << header->endpoints.blue.x << ", " << header->endpoints.blue.y << ", " << header->endpoints.blue.z <<  ")" << std::endl;
    std::cout << "Gamma red: " << header->gamma_red << std::endl;
    std::cout << "Gamma green: " << header->gamma_green << std::endl;
    std::cout << "Gamma blue: " << header->gamma_blue << std::endl;
}

bool Info_Header_V4::validate(void) 
{
    if (header->compression != 0) {
        std::cout << "Following compression type does not supports: " << compression_val2name(header->compression) << std::endl;
        return false;
    }
    if (header->bit_count != 24) {
        std::cout << "Following bit count does not supports: " << header->bit_count << std::endl;
        return false;
    }
    return true;
}

void Info_Header_V4::get_dimensions(uint32_t& width, uint32_t& height)
{
    width = header->width;
    height = header->height;
}
