#ifndef __FILE_CREATOR_HPP__
#define __FILE_CREATOR_HPP__

#include <thread>
#include <Bitmap.hpp>

class File_Creator {
private:
    char *filename;
    
    int num_jobs;

    std::shared_ptr<Bitmap> bitmap;

    std::vector<char> output;

public:
    File_Creator(char *, std::shared_ptr<Bitmap> bmap);

    void process(void);

    void write(void);
};

#endif