## Description

Program image2text processes image in .bmp format and creats 

an equal imaage on the output filled with ASCII characters.


## Compilation:

Project has CmakeLists.txt file and can be compiled in 2 steps:

1) cmake .

2) make

After that there will be file image2text in project directory "cmake-build-debug"


## Program execution:

Program image2text can be launched in command line:

1)cd cmake-build-debug

2)./image2text input_file [output_file] [formatter] (e.g ./image2text ../V1_RGB_24.bmp)


1)input_file is path to the input .bmp file (mandatory)

2)output_file is path to the output .txt file (optional, if no one is provided then default "out.txt" will be chosen)

3)formatter is set of ASCII characters (optional, if no one is provided then default " .:io+#@" will be chosen)


## Demostration:

To demonstrate basic functionality there are some images to process in project directory.

To launch it, firstly, compile the program as described in "Compilation" section, then:

./image2text filename

All test files contains .bmp image types in their names, ex:

V1_RGB_24.bmp stands for:

V1 - bmp information header: Version 1

RGB - compression type: BI_RGB

24 - 24 bits per pixel

This program has limitations:

header versions - 1, 4, 5

compression - BI_RGB only

bits count - 24 bits/pixel only

## Testing

## Input data

BITMAPCOREHEADER:
Signature: BM
File size: 32456 bytes
Bitmap offset: 54 bytes
BITMAPINFOHEADER
Image width: 120 px
Image heigh: 90 px
Bit count: 24 bits/px
Compression: BI_RGB
Image size: 32402 bytes
Resolution horizontal: 2834 px/m
Resolution vertical: 2834 px/m
Colors used: 0
Colors used: 0

## Seconds

real	0m0,007s
user	0m0,004s
sys	    0m0,003s

## CPU

description: CPU
product: Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz
version: Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz


 
