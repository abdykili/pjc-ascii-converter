#include "Bitmap.hpp"
#include <string>

Bitmap::Bitmap(uint32_t w, uint32_t h)
{
    width = w;
    height = h;
    padding = w*3 % 4;
    grayscale = std::vector<std::vector<uint8_t>>(height);
    for (int i = 0; i < height; i++) {
        grayscale[i] = std::vector<uint8_t>(width);
    }
}

static inline unsigned char rgb2gray(unsigned char r, unsigned char g, unsigned char b) {
    return (unsigned char)(((int)r*75 + (int)g*155 + (int)b*25) / 255);
}

void Bitmap::extract_bitmap(std::ifstream& s) 
{
    uint8_t r,g,b;
    for (int i = height - 1; i >= 0; i--) {
        for (int j = 0; j < width; j++) {
            r = static_cast<uint8_t>(s.get());
            g = static_cast<uint8_t>(s.get());
            b = static_cast<uint8_t>(s.get());
            grayscale[i][j] = rgb2gray(r, g, b);
        }
        for (int j = 0; j < padding; j++) {
            s.ignore();
        }
    }
}

void Bitmap::write(std::string& filename, std::string& fmt)
{
    std::ofstream output(filename);
    int idx, denom = 256 / fmt.length();
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            idx = grayscale[i][j] / denom;
            output << fmt[idx] << fmt[idx];
        }
        output << std::endl;
    }
    output.close();
}